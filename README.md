# ift6135-h01

# Problem 1

* Intall dependencies using pip3 install -r requirements.txt
* To run code, python3 -m problem1

# Problem 2
- Refer readme in `p2` directorty.

# Problem 3
- Place data inside `p3/data` directory
- Intall dependencies using `pip3 install -r requirements.txt`
- To run code, `python3 -m p3`
- Results will be generated in file `p3/submission_skip_connection_with_six_blocks_no_jitter_no_vert_flip.csv`

