#!/usr/bin/env python3

import csv
import os

import matplotlib.pyplot as plt
import numpy as np
import parse
import PIL
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
from PIL import Image
from sklearn.model_selection import StratifiedShuffleSplit
from torch.utils.data import random_split
from torchvision import datasets, transforms

from p3.classifier import (BasicClassifier, LinearHeavyClassifier,
                           LittleClassifier, SkipConnectionClassifier,
                           SkipTwoLevelClassifier, ThreeLayerClassifier,
                           TwoHeadedClassifier)
from p3.dataset import ImageFolderWithPaths

np.random.seed(42)
torch.manual_seed(42)
CURR_DIR = os.path.dirname(os.path.realpath(__file__))
DATA_DIR = os.path.join(CURR_DIR, 'data')
TRAINSET_DIR = os.path.join(DATA_DIR, 'trainset/trainset')
TESTSET_DIR = os.path.join(DATA_DIR, 'testset')
MODEL_DIR = os.path.join(CURR_DIR, 'saved_models')
DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
MODEL_PATH = os.path.join(MODEL_DIR, 'model.pt')


def plot_results():
    result_dir = os.path.join(CURR_DIR, 'results')
    data_dict = {}
    for f in os.listdir(result_dir):
        if not f.endswith('.txt'):
            continue

        file_path = os.path.join(result_dir, f)
        with open(file_path, 'r') as fob:
            data = fob.read()
            data_dict[file_path] = data
    train_statement = "Epoch : {} Loss : {} Train Acc: {}"
    validation_statement = "Epoch : {} Test Acc : {}"
    t_s = parse.compile(train_statement)
    v_s = parse.compile(validation_statement)

    for i, (file_path, data) in enumerate(data_dict.items()):
        epochs = []
        train_acc = []
        valid_acc = []
        train_loss = []
        lines = data.split('\n')
        for line in lines:
            match = t_s.parse(line)
            if match is not None:
                epoch, t_loss, t_acc = match
                epochs.append(int(epoch))
                train_acc.append(float(t_acc))
                train_loss.append(float(t_loss))
                continue
            match = v_s.parse(line)
            if match is not None:
                _, v_acc = match
                valid_acc.append(float(v_acc))
        plt.figure(i+1)
        plt.xlabel('Epochs')
        plt.ylabel('Accuracy')
        plt.plot(epochs, train_acc, label='Training accuracy')
        plt.plot(epochs, valid_acc, label='Validation accuracy')
        plt.legend(loc='upper left')
        out_name = os.path.join(result_dir, file_path[:-len('.txt')]+'.png')
        plt.savefig(out_name)


def plot_filters():
    '''
    Visualize first layer weights
    '''
    model_path = os.path.join(
        CURR_DIR, 'saved_models/model.pt_skip_connection_with_six_blocks_no_jitter_no_vert_flip')
    # load the model
    checkpoint = torch.load(model_path, map_location='cpu')
    model_weight = checkpoint['model_state']

    # plot the first layer filters
    plt.figure()
    first_weights = model_weight['cnn.0.conv1.weight']
    for idx in range(first_weights.shape[0]):
        plt.subplot(4, 4, idx + 1)
        plt.imshow(first_weights[idx, ...])
    plt.savefig(os.path.join(CURR_DIR, "results/filter_layer_filter.png"))


def visualize_image_through_layers():
    model_path = os.path.join(
        CURR_DIR, 'saved_models/model.pt_skip_connection_with_six_blocks_no_jitter_no_vert_flip')
    # load the model
    checkpoint = torch.load(model_path, map_location='cpu')
    model_weight = checkpoint['model_state']
    model = SkipConnectionClassifier()
    model.load_state_dict(model_weight)

    data_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    model.eval()
    images = [
        os.path.join(TESTSET_DIR, 'test/2.jpg'),
        os.path.join(TESTSET_DIR, 'test/15.jpg'),
        os.path.join(TESTSET_DIR, 'test/586.jpg'),
    ]
    layer_label = ['Convolution', 'Convolution', 'Final']
    for i, im in enumerate(images):
        image1 = Image.open(im)
        image1 = data_transform(image1)
        image1 = image1.unsqueeze(0)
        input_im = image1
        plt.figure(figsize=(8, 8))
        # plt.figure()
        for idx, module in model.cnn._modules.items():
            oim1 = module.lin1(module.conv1(input_im))
            oim2 = module.conv2(oim1)
            oim = module(input_im)
            out_images = [oim1, oim2, oim]
            input_im = oim

            for layer_num, image in enumerate(out_images):
                image = image.squeeze(0)
                image = torch.sum(image, dim=0)
                image = torch.div(image, image.shape[0])
                ax = plt.subplot(6, 3, int(idx)*3+layer_num + 1)
                ax.set_title("Block {} {}".format(
                    int(idx)+1, layer_label[layer_num]))
                ax.imshow(image.detach().numpy())
            input_im = out_images[-1]
        plt.subplots_adjust(hspace=0.8)
        plt.tight_layout()
        plt.savefig(os.path.join(
            CURR_DIR, "results/layer_output_{}.png".format(i)))
        plt.close()


def find_interesting_images():
    # create dataset
    dataset = get_dataset()
    valid_dataset = get_dataset(False)

    # create dataloader
    _, validation_dataloader = get_dataloader(
        dataset, valid_dataset)

    model_path = os.path.join(
        CURR_DIR, 'saved_models/model.pt_skip_connection_with_six_blocks_no_jitter_no_vert_flip')
    # load the model
    checkpoint = torch.load(model_path, map_location='cpu')
    model_weight = checkpoint['model_state']
    model = SkipConnectionClassifier()
    model.load_state_dict(model_weight)

    model.eval()

    mis_dog = None
    mis_cat = None
    conf = None
    plt.figure()
    for inputs, targets in validation_dataloader:
        outputs = model(inputs)
        confusion = (outputs > -0.1) * (outputs < 0.2)
        dog = outputs > 0
        cat = outputs < 0

        corr_dog = dog.long().eq(targets.data).view(-1).tolist()
        corr_cat = cat.long().eq(targets.data).view(-1).tolist()

        c_index = int(torch.argmax(confusion, dim=0))
        if confusion[c_index] == 1 and conf is None:
            conf = True
            ax = plt.subplot(1, 3, 1)
            ax.imshow(inputs[c_index].permute(1, 2, 0).numpy())
            ax.set_title('50/50')
        for i in range(len(corr_dog)):
            if mis_dog is None and corr_dog[i] == False:
                ax = plt.subplot(1, 3, 2)
                ax.imshow(inputs[i].permute(1, 2, 0).numpy())
                ax.set_title('Misclassified dog')
            elif mis_cat is None and corr_cat[i] == False:
                ax = plt.subplot(1, 3, 3)
                ax.imshow(inputs[i].permute(1, 2, 0).numpy())
                ax.set_title('Misclassified cat')
        if mis_cat and mis_dog and conf:
            break
    plt.tight_layout()
    plt.savefig(os.path.join(CURR_DIR, 'results/interesting.png'))


def get_dataset(transform=True):
    # define some transformations
    data_transform = transforms.Compose([
        # transforms.ColorJitter(),
        transforms.RandomRotation(30, resample=PIL.Image.NEAREST),
        transforms.RandomHorizontalFlip(),
        # transforms.RandomVerticalFlip(),
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
    # define the whole training dataset
    if transform is False:
        data_transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
    dataset = datasets.ImageFolder(root=TRAINSET_DIR,
                                   transform=data_transform)

    print("Dataset length: {}".format(len(dataset)))
    return dataset


def get_dataloader(dataset, valid_dataset):
    # not using complicated sampling yet
    # can be improved though
    split_percentage = 0.02

    # use StratifiedShuffle and get the indices
    total_len = len(dataset)
    fake_X = np.zeros(total_len)
    fake_y = [x[1] for x in dataset.samples]

    ss = StratifiedShuffleSplit(
        n_splits=1, test_size=split_percentage, random_state=0)
    train_indices, validation_indices = list(ss.split(fake_X, fake_y))[0]

    train_sampler = torch.utils.data.SubsetRandomSampler(train_indices)
    validation_sampler = torch.utils.data.SubsetRandomSampler(
        validation_indices)
    train_dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=20, shuffle=False,
        sampler=train_sampler, num_workers=4)
    validation_dataloader = torch.utils.data.DataLoader(
        valid_dataset, batch_size=20, shuffle=False,
        sampler=validation_sampler, num_workers=4)

    return train_dataloader, validation_dataloader


def train_model(
        train_dataloader,
        validation_dataloader,
        model,
        optimizer,
        criterion,
        epochs=10,
        exp_name='exp'):
    for epoch in range(1, epochs):
        losses = []

        # Train
        model.train()
        total = 0
        correct = 0
        for _, (inputs, targets) in enumerate(train_dataloader):
            inputs, targets = inputs.to(DEVICE), targets.to(DEVICE)
            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, targets.float())
            loss.backward()
            optimizer.step()
            losses.append(loss.item())
            with torch.no_grad():
                p = outputs > 0
                correct += p.long().eq(targets).sum()
                total += targets.size(0)
        print('Epoch : %d Loss : %f Train Acc: %f' %
              (epoch, np.mean(losses), 100*correct/total))

        if epoch % 50 == 0:
            for pg in optimizer.param_groups:
                pg['lr'] = pg['lr']/10

        if epoch % 10 == 9:
            torch.save({
                'epoch': epoch,
                'model_state': model.state_dict(),
                'optimizer_state': optimizer.state_dict(),
                'label_mapping': train_dataloader.dataset.class_to_idx,
                'loss': loss,
            }, os.path.join(MODEL_DIR, "latest_train_{}.pt".format(exp_name)))
        # Evaluate
        model.eval()
        total = 0
        correct = 0
        for _, (inputs, targets) in enumerate(validation_dataloader):
            inputs, targets = inputs.to(DEVICE), targets.to(DEVICE)
            predicted = model(inputs)
            # _, predicted = torch.max(outputs.data, 1)
            predicted = predicted > 0
            total += targets.size(0)
            correct += predicted.long().eq(targets.data).cpu().sum()

        print('Epoch : %d Test Acc : %.3f' % (epoch, 100.*correct/total))
        print('--------------------------------------------------------------')
    torch.save({
        'label_mapping': train_dataloader.dataset.class_to_idx,
        'model_state': model.state_dict()
    }, "{}_{}".format(MODEL_PATH, exp_name))


def train(model, lr, exp_name):

    # create dataset
    dataset = get_dataset()
    valid_dataset = get_dataset(False)

    # create dataloader
    train_dataloader, validation_dataloader = get_dataloader(
        dataset, valid_dataset)

    clf = model.to(DEVICE)

    optimizer = torch.optim.SGD(clf.parameters(), lr=lr)
    criterion = nn.BCEWithLogitsLoss()

    train_model(
        train_dataloader,
        validation_dataloader,
        clf,
        optimizer,
        criterion,
        epochs=100,
        exp_name=exp_name)


def test(model, exp_name='exp'):
    # get dataset
    data_transform = transforms.Compose([
        transforms.ToTensor(),
        transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    test_dataset = ImageFolderWithPaths(root=TESTSET_DIR,
                                        transform=data_transform)

    # get dataloader
    test_loader = torch.utils.data.DataLoader(
        test_dataset, batch_size=20, shuffle=False, num_workers=4)

    # load the model
    checkpoint = torch.load("{}_{}".format(MODEL_PATH, exp_name))

    model.load_state_dict(checkpoint['model_state'])
    model.to(DEVICE)
    model.eval()

    # make reverse mapping
    label_to_class = {v: k for k, v in checkpoint['label_mapping'].items()}

    output_array = []
    for inputs, targets, paths in test_loader:
        inputs, targets, = inputs.to(DEVICE), targets.to(DEVICE)
        outputs = model(inputs)
        predicted = outputs > 0
        output_array.extend(
            (os.path.basename(paths[i])[:-4],
             label_to_class[int(predicted[i])])
            for i in range(len(predicted)))

    out_file = os.path.join(CURR_DIR, 'submission_{}.csv'.format(exp_name))
    with open(out_file, 'w') as fob:
        csv_out = csv.writer(fob)
        csv_out.writerow(("id", "label"))
        for row in output_array:
            csv_out.writerow(row)


def exp1():
    model = BasicClassifier()
    lr = 0.1
    exp_name = 'exp1'
    train(model, lr, exp_name)
    test(model, exp_name)


def exp2():
    model = LinearHeavyClassifier()
    lr = 0.1
    exp_name = 'exp2'
    train(model, lr, exp_name)
    test(model, exp_name)


def exp3():
    model = LittleClassifier()
    lr = 0.1
    exp_name = 'exp3'
    train(model, lr, exp_name)
    test(model, exp_name)


def exp4():
    model = ThreeLayerClassifier()
    lr = 0.1
    exp_name = 'three_layer_classifier'
    train(model, lr, exp_name)
    test(model, exp_name)


def exp5():
    model = TwoHeadedClassifier()
    lr = 0.1
    exp_name = 'two_headed_classifier'
    train(model, lr, exp_name)
    test(model, exp_name)


def exp6(is_train=True):
    model = SkipConnectionClassifier()
    exp_name = 'skip_connection_with_six_blocks_no_jitter_no_vert_flip'
    if is_train:
        lr = 0.1
        train(model, lr, exp_name)
    test(model, exp_name)


def exp7():
    model = SkipTwoLevelClassifier()
    lr = 0.1
    exp_name = 'skip_two_level_with_six_blocks'
    train(model, lr, exp_name)
    test(model, exp_name)


def main():
    exp6(is_train=False)


if __name__ == '__main__':
    main()
