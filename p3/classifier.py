import torch.nn.functional as F
from torch import nn


class BasicClassifier(nn.Module):
    """Convnet BasicClassifier"""

    def __init__(self):
        super(BasicClassifier, self).__init__()
        self.conv = nn.Sequential(
            # Layer 1
            nn.Conv2d(in_channels=3, out_channels=16,
                      kernel_size=(3, 3), padding=1),
            # nn.Dropout(p=0.5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2), stride=2),

            # Layer 2
            nn.Conv2d(in_channels=16, out_channels=32,
                      kernel_size=(3, 3), padding=1),
            # nn.Dropout(p=0.5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2), stride=2),

            # Layer 3
            nn.Conv2d(in_channels=32, out_channels=64,
                      kernel_size=(3, 3), padding=1),
            # nn.Dropout(p=0.5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2), stride=2),

            # Layer 4
            nn.Conv2d(in_channels=64, out_channels=128,
                      kernel_size=(3, 3), padding=1),
            # nn.Dropout(p=0.5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2), stride=2),

            # Layer 5
            nn.Conv2d(in_channels=128, out_channels=256,
                      kernel_size=(3, 3), padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2), stride=2),

            # Layer 6
            nn.Conv2d(in_channels=256, out_channels=512,
                      kernel_size=(3, 3), padding=1),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2), stride=2)

        )
        # Logistic Regression
        self.clf = nn.Sequential(
            nn.Linear(512, 32),
            nn.ReLU(),
            nn.Linear(32, 1)
        )

        self._init_weights()

    def _init_weights(self):
        for module in self.modules():
            if isinstance(module, nn.Conv2d):
                nn.init.kaiming_normal_(module.weight, nonlinearity='relu')
            elif isinstance(module, nn.Linear):
                nn.init.normal_(module.weight, 0, 0.01)
                nn.init.constant_(module.bias, 0)

    def forward(self, x):
        conv_out = self.conv(x)
        conv_out = conv_out.view(-1, 512)
        return self.clf(conv_out).view(-1)


class LittleClassifier(nn.Module):
    def __init__(self):
        super(LittleClassifier, self).__init__()

        self.conv = nn.Sequential(
            nn.Conv2d(
                in_channels=3, out_channels=16, kernel_size=3, padding=1
            ),
            nn.ReLU(),
            nn.Conv2d(
                in_channels=16, out_channels=16, kernel_size=3, padding=1
            ),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2)),

            nn.Conv2d(
                in_channels=16, out_channels=32, kernel_size=3, padding=1
            ),
            nn.ReLU(),
            nn.Conv2d(
                in_channels=32, out_channels=32, kernel_size=3, padding=1
            ),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2)),

            nn.Conv2d(
                in_channels=32, out_channels=64, kernel_size=3, padding=1
            ),
            nn.ReLU(),
            nn.Conv2d(
                in_channels=64, out_channels=64, kernel_size=3, padding=1
            ),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2)),

            nn.Conv2d(
                in_channels=64, out_channels=128, kernel_size=3, padding=1
            ),
            nn.ReLU(),
            nn.Conv2d(
                in_channels=128, out_channels=128, kernel_size=3, padding=1
            ),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2)),

            nn.Conv2d(
                in_channels=128, out_channels=256, kernel_size=3, padding=1
            ),
            nn.ReLU(),
            nn.Conv2d(
                in_channels=256, out_channels=256, kernel_size=3, padding=1
            ),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2)),

            nn.Conv2d(
                in_channels=256, out_channels=512, kernel_size=3, padding=1
            ),
            nn.ReLU(),
            nn.Conv2d(
                in_channels=512, out_channels=512, kernel_size=3, padding=1
            ),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2)),

        )
        self.lin = nn.Sequential(
            nn.Linear(512, 32),
            nn.ReLU(),
            nn.Linear(32, 1),
        )

    def forward(self, x):
        x = self.conv(x)
        x = x.view(-1, 512)
        x = self.lin(x)
        return x.view(-1)


class LinearHeavyClassifier(nn.Module):
    def __init__(self):
        super(LinearHeavyClassifier, self).__init__()

        self.conv = nn.Sequential(
            # Layer 1
            nn.Conv2d(in_channels=3, out_channels=16,
                      kernel_size=(3, 3), padding=1),
            # nn.Dropout(p=0.5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2), stride=2),

            # Layer 2
            nn.Conv2d(in_channels=16, out_channels=32,
                      kernel_size=(3, 3), padding=1),
            # nn.Dropout(p=0.5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2), stride=2),

            # Layer 3
            nn.Conv2d(in_channels=32, out_channels=64,
                      kernel_size=(3, 3), padding=1),
            # nn.Dropout(p=0.5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2), stride=2)
        )

        self.linear = nn.Sequential(
            nn.Linear(8*8*64, 8*64),
            nn.ReLU(),
            nn.Linear(8*64, 16),
            nn.ReLU(),
            nn.Linear(16, 1)
        )

    def forward(self, x):
        conv_out = self.conv(x)
        conv_out = conv_out.view(-1, 8*8*64)
        lin_out = self.linear(conv_out)
        return lin_out.view(-1)


class ThreeLayerClassifier(nn.Module):
    def __init__(self):
        super(ThreeLayerClassifier, self).__init__()
        self.conv1 = nn.Conv2d(3, 32, kernel_size=5, padding=1, stride=2)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(32, 32, kernel_size=5, padding=1, stride=2)
        self.conv3 = nn.Conv2d(32, 64, kernel_size=5, padding=1, stride=2)
        self.fc1 = nn.Linear(64 * 7 * 7, 120)
        self.fc2 = nn.Linear(120, 32)
        self.fc3 = nn.Linear(32, 1)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = x.view(-1, 64*7*7)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x.view(-1)


class TwoHeadedClassifier(nn.Module):
    def __init__(self):
        super(TwoHeadedClassifier, self).__init__()
        self.head1 = nn.Sequential(
            # Layer 1
            nn.Conv2d(in_channels=3, out_channels=16,
                      kernel_size=(3, 3), padding=1),
            # nn.Dropout(p=0.5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2), stride=2),

            # Layer 2
            nn.Conv2d(in_channels=16, out_channels=32,
                      kernel_size=(3, 3), padding=1),
            # nn.Dropout(p=0.5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2), stride=2),

            # Layer 3
            nn.Conv2d(in_channels=32, out_channels=64,
                      kernel_size=(3, 3), padding=1),
            # nn.Dropout(p=0.5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2), stride=2),
        )
        self.head2 = nn.Sequential(
            # Layer 1
            nn.Conv2d(in_channels=3, out_channels=16,
                      kernel_size=(3, 3), padding=1),
            # nn.Dropout(p=0.5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2), stride=2),

            # Layer 2
            nn.Conv2d(in_channels=16, out_channels=32,
                      kernel_size=(3, 3), padding=1),
            # nn.Dropout(p=0.5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2), stride=2),

            # Layer 3
            nn.Conv2d(in_channels=32, out_channels=64,
                      kernel_size=(3, 3), padding=1),
            # nn.Dropout(p=0.5),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=(2, 2), stride=2),
        )
        self.clf = nn.Sequential(
            nn.Linear(4096, 512),
            nn.ReLU(),
            nn.Linear(512, 32),
            nn.ReLU(),
            nn.Linear(32, 1)
        )

    def forward(self, x):
        x1 = self.head1(x)
        x2 = self.head2(x)
        x = x1+x2
        x = x.view(-1, 4096)
        x = self.clf(x)
        return x.view(-1)


class BasicSkip(nn.Module):
    def __init__(self, in_channel, out_channel, kernel_size=3):
        super(BasicSkip, self).__init__()
        self.conv1 = nn.Conv2d(in_channel, out_channel,
                               kernel_size=kernel_size, padding=1)
        self.lin1 = nn.ReLU()
        self.conv2 = nn.Conv2d(out_channel, out_channel,
                               kernel_size=kernel_size, padding=1)
        self.conv3 = nn.Conv2d(in_channel, out_channel, kernel_size=1)
        self.lin2 = nn.ReLU()
        self.max_pool = nn.MaxPool2d(kernel_size=2)

    def forward(self, x):
        y = self.lin1(self.conv1(x))
        y = self.conv2(y)
        x = self.conv3(x)
        x += y
        x = self.lin2(x)
        x = self.max_pool(x)
        return x


class SkipConnectionClassifier(nn.Module):
    def __init__(self):
        super(SkipConnectionClassifier, self).__init__()
        self.cnn = nn.Sequential(
            BasicSkip(3, 16),
            BasicSkip(16, 32),
            BasicSkip(32, 64),
            BasicSkip(64, 128),
            BasicSkip(128, 256),
            BasicSkip(256, 512)
        )

        self.clf = nn.Sequential(
            nn.Linear(512, 32),
            nn.ReLU(),
            nn.Linear(32, 1)
        )

    def forward(self, x):
        x = self.cnn(x)
        x = x.view(-1, 512)
        x = self.clf(x)
        return x.view(-1)


class TwoLevelSkip(nn.Module):
    def __init__(self, in_channel, out_channel, kernel_size=3):
        super(TwoLevelSkip, self).__init__()
        self.conv1 = nn.Conv2d(in_channel, out_channel,
                               kernel_size=kernel_size, padding=1)
        self.lin1 = nn.ReLU()

        self.conv2 = nn.Conv2d(out_channel, out_channel,
                               kernel_size=kernel_size, padding=1)
        self.lin2 = nn.ReLU()

        self.conv3 = nn.Conv2d(out_channel, out_channel,
                               kernel_size=kernel_size, padding=1)

        self.conv4 = nn.Conv2d(in_channel, out_channel, kernel_size=1)
        self.lin3 = nn.ReLU()
        self.max_pool = nn.MaxPool2d(kernel_size=2)

    def forward(self, x):
        y = self.lin1(self.conv1(x))
        y1 = self.conv2(y)
        y = self.lin2(y1)
        y2 = self.conv3(y)
        x = self.conv4(x)
        x = x + y1 + y2
        x = self.lin3(x)
        x = self.max_pool(x)
        return x


class SkipTwoLevelClassifier(nn.Module):
    def __init__(self):
        super(SkipTwoLevelClassifier, self).__init__()
        self.cnn = nn.Sequential(
            TwoLevelSkip(3, 16),
            TwoLevelSkip(16, 32),
            TwoLevelSkip(32, 64),
            TwoLevelSkip(64, 128),
            TwoLevelSkip(128, 256),
            TwoLevelSkip(256, 512)
        )

        self.clf = nn.Sequential(
            nn.Linear(512, 32),
            nn.ReLU(),
            nn.Linear(32, 1)
        )

    def forward(self, x):
        x = self.cnn(x)
        x = x.view(-1, 512)
        x = self.clf(x)
        return x.view(-1)
